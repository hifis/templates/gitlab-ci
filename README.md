<!--
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: MIT
-->

# GitLab CI Templates
This is a collection of reusable templates for GitLab CI.

## Templates

### `reuse-compliance`

See [/templates/reuse-compliance.yml](/templates/reuse-compliance.yml)

### `super-linter` (Github Super Linter)

See [templates/lint/super-linter.yml](templates/lint/super-linter.yml)

### `docker`

Documentation: [docs/docker.md](docs/docker.md)
Template: [templates/docker.yml](templates/docker.yml)

## License
MIT
