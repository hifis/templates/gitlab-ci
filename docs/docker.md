<!--
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

SPDX-License-Identifier: MIT
-->

# Using the Docker CI Template

The Docker CI template is provided to allow users to build Docker containers for the internal Gitlab registry.
Conceptually, these templates define a build job (`.docker-build`),
along with a number of different tagging jobs that serve different purposes,
as well as a general purpose test job for running tests on the built images.
By combining these different jobs, we can achieve different results without doing a rebuild for each job.

## The Build Job

The `.docker-build` job must be run before any of the tagging or test jobs are run.
The build job exports an artifact which is a bundled, compressed tarball of the image built.
This artifact can be used for other purposes, but its main use is to be passed to the other jobs,
and also to act as a cache to prevent too much rebuilding.

To use the build job, create a job in your CI file that extends the `.docker-build` job provided by this template.
For example:

```yml
include:
    # include the Docker CI template to access the template jobs
    - "https://gitlab.com/hifis/templates/gitlab-ci/-/raw/master/templates/docker.yml"

stages:
    - build

# create the job that will build the Docker image
docker-build:
    extends: .docker-build
    stage: build
```

## The Tagging Jobs

There are four different tagging jobs, described later in this document.
It is possible to use them in any combination together.
These jobs unzip the exported image provided by the build job,
give it a particular tag,
then upload that tag to the registry.

To use these jobs, create a job in your CI file that extends one of them,
ensure that it will run in a later stage than the build job,
and ensure that it depends on the build job created beforehand.
For example:

```yml
include:
    - "https://gitlab.com/hifis/templates/gitlab-ci/-/raw/master/templates/docker.yml"

stages:
    - build
    - release

docker-build:
    extends: .docker-build
    stage: build

docker-tag-latest:
    extends: .docker-tag-latest
    stage: release
    needs:
        - job: docker-build
          artifacts: true
```

The jobs are as follows:

- **The "always latest" job (`.docker-tag-latest`)**:
    Runs every time the master branch is updated, and pushes to the `latest` tag,
    ensuring that it is always up-to-date and containing the latest code changes.
- **The "only versions" job (`.docker-tag-versions`)**:
    Runs when a new _git_ tag is created (this can be done using the command-line and pushed to the Gitlab repository).
    This git tag should represent a new version of the software being released,
    and a docker tag will be created with the same name as the git version.
- **The "snapshots" job (`.docker-tag-snapshots`)**:
    This runs on every commit, and updates a "snapshot" tag,
    i.e. a tag that represents the current branch and the changes made on it.
    This can be useful if different team members are working in different branches,
    and want to test their code separately without creating new versions.
    However, snapshot tags will not be deleted, even after the branch has been deleted,
    so if you use this job, you should manually prune the snapshot tags in the registry every so often
    to prevent the registry size from growing too much.
- **The "custom tag" job (`.docker-tag-custom`)**:
    This job is for when the other tagging jobs do not serve your needs.
    When this runs, a docker image tag will be created with the value of the variable `$TAG_NAME`,
    which can be set in the `variables` section of the job configuration.
    This job can be extended multiple times, if there are particular needs for your project.

An example using these different jobs appears at the end of this document.

## The Test Job

Finally, there is a testing job that can be used to validate that the built docker artifact works as expected.
(It may also be suitable to run tests beforehand using the source code, but this offers a last failsafe to ensure that the job runs.)
To use the testing job, extend from `.docker-test`, as with the other jobs,
ensure it depends on the build job, and has a separate stage to this job,
and override the `script` key to provide your own tests.
Within this job, the docker image name `$CI_REGISTRY_IMAGE` can be used.

For example:

```yml
include:
    - "https://gitlab.com/hifis/templates/gitlab-ci/-/raw/master/templates/docker.yml"

stages:
    - build
    - test

docker-build:
    extends: .docker-build
    stage: build

post-build-validation:
    extends: .docker-test
    stage: test
    needs:
        - job: docker-build
          artifacts: true
    script:
        - docker run $CI_REGISTRY_IMAGE > output.txt
        - "cat output.txt | grep 'Analysis Complete: SUCCESS'"
```

## A Complete Example

```yml
include:
    - "https://gitlab.com/hifis/templates/gitlab-ci/-/raw/master/templates/docker.yml"

stages:
    - build
    - test
    - release

docker-build:
    extends: .docker-build
    variables:
        # handle Dockerfile in an odd location
        DOCKERFILE: "./src/Dockerfile.release"
    stage: build

post-build-validation:
    extends: .docker-test
    stage: test
    needs:
        - job: docker-build
          artifacts: true
    script:
        - "cat testdata.json | docker run $CI_REGISTRY_IMAGE > output.txt"
        - "cat output.txt | grep 'Analysis Complete: SUCCESS'"

release-latest:
    extends: .docker-tag-latest
    stage: release
    needs:
        # fetch the latest build artifacts
        - job: docker-build
          artifacts: true
        # don't release anything until we've validated the build
        # (note that we don't need artifacts from this job)
        - job: post-build-validation
          artifacts: false

# this job could be scheduled via scheduled pipelines
# to run once a day and produce a latest version for people to use
nightly-release:
    extends: .docker-tag-custom
    stage: release
    variables:
        TAG_NAME: "nightly"
    only: ["schedules"]
    needs:
        - job: docker-build
          artifacts: true
        - job: post-build-validation
          artifacts: false
```
